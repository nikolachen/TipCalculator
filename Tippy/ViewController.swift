//
//  ViewController.swift
//  Tippy
//
//  Created by Jinhua on 12/20/17.
//  Copyright © 2017 Jinhua Chen. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var billField: UITextField!
    @IBOutlet weak var tipControl: UISegmentedControl!
    @IBOutlet weak var tipSlider: UISlider!
    @IBOutlet weak var tipView: UILabel!
    @IBOutlet weak var people: UITextField!
    @IBOutlet weak var split: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func onTap(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func calculateTipButton(_ sender: UISegmentedControl) {
        
//        print("running button")
        let tipPercentage = [0.15, 0.2, 0.25]
        
        let bill = Double(billField.text!) ?? 0
        let num = Int(people.text!) ?? 1
        let tip = bill * tipPercentage[tipControl.selectedSegmentIndex]
        let total = bill + tip
        let spl = total / Double(num)
        
        tipSlider.value = Float(tipPercentage[tipControl.selectedSegmentIndex])
        tipView.text = String(format: "%.2f%%", tipPercentage[tipControl.selectedSegmentIndex] * 100)
        tipLabel.text = String(format: "$%.2f", tip)
        totalLabel.text = String(format: "$%.2f", total)
        split.text = String(format: "$%.2f", spl)
    }
    
    @IBAction func calculateTipSlider(_ sender: UISlider) {
        
//        print("running slider")
        let tipPercentage = Double(tipSlider.value)

        let bill = Double(billField.text!) ?? 0
        let num = Int(people.text!) ?? 1
        let tip = bill * tipPercentage
        let total = bill + tip
        let spl = total / Double(num)

        tipView.text = String(format: "%.2f%%", tipPercentage * 100)
        tipLabel.text = String(format: "$%.2f", tip)
        totalLabel.text = String(format: "$%.2f", total)
        split.text = String(format: "$%.2f", spl)
    }
    
    
    
//    Some test code for setting page
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        print("view will appear")
//        // This is a good place to retrieve the default tip percentage from UserDefaults
//        // and use it to update the tip amount
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        print("view did appear")
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        print("view will disappear")
//    }
//
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        print("view did disappear")
//    }
//
    
}

